Jeu de Hex

**Règles**

Les règles du jeu de Hex sont très simples, pourtant ce jeu présente une grande
richesse de tactiques et de stratégies. Il a été également l’objet de modélisations
mathématiques.

• Nombre de joueurs : 2

• Matériel : Un plateau de jeu, des pions blancs et des pions noirs. Le plateau
du jeu de Hex est composé de cases hexagonales formant un losange. La
taille du plateau peut varier (habituellement 6x6, 9x9 ou 11x11). Deux cotés
opposés du losange sont de couleurs différentes. (blanc et noir par convention)

• Déroulement : Le joueur blanc commence. Les joueurs jouent chacun leur
tour. À chaque tour, le joueur place un pion de sa couleur sur une case libre
du plateau. Le premier joueur qui réussit à relier ses deux bords par un
chemin de pions contigus de sa couleur a gagné. Il ne peut y avoir qu'un pion
par case. Les pions posés le sont définitivement, ils ne peuvent être ni retirés,
ni déplacés.

Une règle supplémentaire est appliquée (seulement après quelques parties
d’apprentissage). Si Blanc commence, il joue le premier coup, Noir a alors le
choix entre jouer à son tour normalement ou intervertir le pion que vient de
poser Blanc avec un de ses propres pions. Le jeu continue ensuite
normalement. Cela impose de jouer un premier coup ni trop fort (car donnerait
un avantage au joueur adverse qui se l'approprierait) ni trop faible (il serait
alors laissé par l'adversaire) et réduit l'avantage de commencer. Cette règle est surnommé la règle du gâteau (pie rule).

(source des règles : Académie de Nantes)
